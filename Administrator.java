import java.util.*;
import java.sql.*;

public class Administrator extends User {
    public Administrator(int id, String user)
    {
        // credentials no real data just user name and other user data
        userID = id;
        username = user;
        type = "Administrator";
    }

    public void createTeacher( String name )
    {
        // Insert teacher into db
    }

    public void createStudent( String name )
    {
        // insert student into db
    }

    public void createCourse( String name )
    {
        // insert course into db
    }

    public void assignTeacher( Teacher teacher, Course course )
    {
        // assign teacher to course

        // relationship
    }

    public void assignStudent( String course )
    {
        // assign student to course

        // relationship
    }
}
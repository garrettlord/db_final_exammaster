public class Answer {
    // Properties
    int answerID;
    int questionID;

    String answer;

    public Answer()
    {
        
    }

    public int answerID()
    {
        return answerID;
    }

    public void setAnswerID( int id )
    {
        answerID = id;
    }

    public int questionID()
    {
        return questionID;
    }

    public void setQuestionID( int id )
    {
        questionID = id;
    }

    public String answer()
    {
        return answer;
    }

    public void setAnswer( String txt )
    {
        answer = txt;
    }
}
import java.util.*;
import java.sql.*;

public class Course {
    // Properties
    int courseID;
    int teacherID;
    String name;

    List<Exam> exams;

    public Course(int course, int t, String n)
    {
        courseID = course;
        teacherID = t;
        name = n;
        exams = new ArrayList<Exam>();
    }

    public int courseID()
    {
        return courseID;
    }

    public void setCourseID( int id )
    {
        courseID = id;
    }

    public int teacherID()
    {
        return teacherID;
    }

    public void setTeacherID( int id )
    {
        teacherID = id;
    }

    public String name()
    {
        return name;
    }

    public void setName( String user )
    {
        name = user;
    }
}
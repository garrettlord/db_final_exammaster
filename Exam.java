import java.util.*;
import java.sql.*;

public class Exam {
    // Properties
    int examID;
    int courseID;
    String _name;
    
    List<Question> questions;

    public Exam(int id, int classN, String ExamName)
    {
        examID = id;
        courseID = classN;
        _name = ExamName;
    }

    public int courseID()
    {
        return courseID;
    }

    public void setCourseID( int id )
    {
        courseID = id;
    }

    public int examID()
    {
        return examID;
    }

    public void setExamID( int id )
    {
        examID = id;
    }

    public String name()
    {
        return _name;
    }

    public void setName( String user )
    {
        _name = user;
    }
}
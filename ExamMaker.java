import javax.swing.*;
import javax.swing.border.TitledBorder;
import java.awt.*;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.*;
import java.lang.*;
import java.sql.*;
import java.util.List;

public class ExamMaker extends JFrame {
    public Connection con;

    private JPanel rootPanel;
    private JPanel loginPanel;
    private JPanel debugPanel;
    private JPanel adminController;
    private JPanel createTeacherInput;
    private JPanel createStudentInput;
    private JPanel createClassInput;
    private JPanel teacherController;
    private JPanel createExam;
    private JPanel CheckStudentExams;
    private JPanel addQuestionsToExam;
    private JPanel studentResultGrades;
    private JPanel StudentsController;
    private JPanel TakeExam;

    private JTextPane textPane;
    private JButton testButton;
    private JButton loginButton;
    private JTextField usernameTextField;
    private JPasswordField passwordTextField;
    private JButton button1;
    private JButton goToCreateTeacherButton;
    private JButton goToCreateStudentButton;
    private JButton goToCreateClassButton;
    private JButton backToLoginButton;
    private JTextField studentNameTextField;
    private JButton backToAdminControllerButton;
    private JTextField teacherNameTextField;
    private JButton backToAdminControllerButton1;
    private JButton backToAdminControllerButton2;
    private JTextField classNameTextField;
    private JTextField PasswordForStudentTextField;
    private JTextField PasswordForTeacherTextField;
    private JButton goToCheckStudentGradesButton;
    private JButton goToCreateExamButton;
    private JTextField examTitleTextField;
    private JTextField questionTextField;
    private JTextField answerTextField;
    private JButton createExamBackToTeacherAdmin;
    private JTextField classTakingExamTextField;
    private JTextField question2TextField;
    private JTextField answer2TextField;
    private JTextField question3TextField;
    private JTextField answer3TextField;
    private JTextField question4TextField;
    private JTextField answer4TextField;
    private JTextField question5TextField;
    private JTextField answer5TextField;
    private JComboBox comboBox1;
    private JButton checkGradesButton;
    private JTextPane textPane1;
    private JComboBox studentToClassBox2;
    private JComboBox classSelected;
    private JButton addTeacherOrStudentButton;
    private JButton takeExamButton;
    private JButton checkExamScoresButton;
    private JTextField textField1;
    private JTextField textField2;
    private JTextField textField3;
    private JTextField textField4;
    private JTextField textField5;
    private JTextField textField7;
    private JTextField textField8;
    private JTextField textField9;
    private JTextField textField10;
    private JTextField textField11;
    private JButton submitExamButton;
    private JTextArea pleaseAnswerTheQuestionsTextArea;
    private JComboBox comboBox2;
    private JTextArea textArea1;
    private JButton button2;
    private JButton goBackToDashboardButton;
    private JComboBox classTakingBox;
    private JPanel TeacherGradeView;
    private JTextArea textArea2;
    private JButton goBackToDashboardButton1;
    private JButton logoutButton;
    private JButton logOutButton;
    private JButton moveToAddQuestionsButton;

    public User currentUser;

    public static void main(String args[]) {
        ExamMaker examer = new ExamMaker();
    }

    /*
     * Constructor for DB Accessor
    */
    public ExamMaker() {
        setContentPane(rootPanel);
        debugPanel.setVisible(false);
        adminController.setVisible(false);
        createStudentInput.setVisible(false);
        createTeacherInput.setVisible(false);
        createClassInput.setVisible(false);
        teacherController.setVisible(false);
        createExam.setVisible(false);
        CheckStudentExams.setVisible(false);
        TakeExam.setVisible(false);
        StudentsController.setVisible(false);
        studentResultGrades.setVisible(false);
        addQuestionsToExam.setVisible(false);
        TeacherGradeView.setVisible(false);


        pack();

        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setDefaultLookAndFeelDecorated(true);

        try {
            con = connect();
        } catch (Exception e) {
            e.printStackTrace();
            System.exit(1);
        }

        loginButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                String name = usernameTextField.getText();
                String pass = passwordTextField.getText();

//                loginPanel.setVisible(false);
//                adminController.setVisible(true);

                boolean loginAttempt = login(name, pass);
                loginAttempt = true;
                if (loginAttempt) {
                    usernameTextField.setText("Username");
                    passwordTextField.setText("Password");
                    String type = currentUser.type;
                    loginPanel.setVisible(false);
                    if (type.equals("Administrator")) {
                        updateDisplayItems();
                        adminController.setVisible(true);
                    } else if (type.equals("Teacher")) {
                        System.out.print("IN TEACHER");

                        teacherController.setVisible(true);

                    } else if (type.equals("Student")) {
                        updateDisplayItems();
                        StudentsController.setVisible(true);
                    }
                } else {
                    System.out.printf("Failed to login %s\n", name);
                }
            }
        });

        takeExamButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                StudentsController.setVisible(false);
                TakeExam.setVisible(true);
                String examTitle = comboBox2.getSelectedItem().toString();

                Exam e = getExamWithName(examTitle);

                ArrayList<Question> q = (ArrayList<Question>) getQuestionsForExam(e);
                textField1.setText(q.get(0)._text);
                textField2.setText(q.get(1)._text);
                textField3.setText(q.get(2)._text);
                textField4.setText(q.get(3)._text);
                textField5.setText(q.get(4)._text);

                textField7.setText("");
                textField8.setText("");
                textField9.setText("");
                textField10.setText("");
                textField11.setText("");
            }
        });


        submitExamButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                String examTitle = comboBox2.getSelectedItem().toString();

                //TODO get exam questions by examTitle above and write to questions in the order below
                textField1.setText("Question 1");
                textField2.setText("Question 2");
                textField3.setText("Question 3");
                textField4.setText("Question 4");
                textField5.setText("Question 5");

                String answer1 = textField7.getText();
                String answer2 = textField8.getText();
                String answer3 = textField9.getText();
                String answer4 = textField10.getText();
                String answer5 = textField11.getText();

                Exam e = getExamWithName(examTitle);
                ArrayList<Question> q = (ArrayList<Question>) getQuestionsForExam(e);


                // q get queues for exam and questions

//                textArea1.setText("You got a 60% on Exam 1!\nYou should try a little harder");

                // Make Exam_response calc grade
//                updateQuestion(q.get(0));
//                updateQuestion(q.get(1));
//                updateQuestion(q.get(2));
//                updateQuestion(q.get(3));
//                updateQuestion(q.get(4));


                ExamResponse er = new ExamResponse(0, 60.0);
                er.examID = e.examID;
                er.userID = currentUser.userID;

                insertEResponse(er);
                ExamResponse er2 = getEResponseByExam(e);

                int total = 0;

                QuestionResponse qr = new QuestionResponse();
                qr.questionID = q.get(0).questionID;
                qr.examResponseID = er2.examResponseID;
                qr.answer = answer1;
                qr.correct = (answer1.equals(q.get(0)._answer));
                if (qr.correct) {
                    total += 20;
                }
                insertQResponse(qr);

                qr = new QuestionResponse();
                qr.questionID = q.get(1).questionID;
                qr.examResponseID = er2.examResponseID;
                qr.answer = answer2;
                qr.correct = (answer2.equals(q.get(1)._answer));
                if (qr.correct) {
                    total += 20;
                }
                insertQResponse(qr);

                qr = new QuestionResponse();
                qr.questionID = q.get(2).questionID;
                qr.examResponseID = er2.examResponseID;
                qr.answer = answer3;
                qr.correct = (answer2.equals(q.get(2)._answer));
                if (qr.correct) {
                    total += 20;
                }
                insertQResponse(qr);

                qr = new QuestionResponse();
                qr.questionID = q.get(3).questionID;
                qr.examResponseID = er2.examResponseID;
                qr.answer = answer4;
                qr.correct = (answer3.equals(q.get(3)._answer));
                if (qr.correct) {
                    total += 20;
                }
                insertQResponse(qr);

                qr = new QuestionResponse();
                qr.questionID = q.get(4).questionID;
                qr.examResponseID = er2.examResponseID;
                qr.answer = answer5;
                qr.correct = (answer4.equals(q.get(4)._answer));
                if (qr.correct) {
                    total += 20;
                }
                insertQResponse(qr);

                JFrame frame = new JFrame("FrameDemo");
                JOptionPane.showMessageDialog(frame, String.format("You scored a %d%%", total));

                //TODO submit answers for a grade into SQL
                TakeExam.setVisible(false);
                //TODO populate string of all grade info
//                textArea1.setText("RESULTS OF GRADES SET BOX FROM RETURN OF all grades");
                studentResultGrades.setVisible(true);
            }
        });

        addTeacherOrStudentButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                String studentOrTeacherSelected = studentToClassBox2.getSelectedItem().toString();
                String _class = classSelected.getSelectedItem().toString();
                assignCourseToUser(_class, studentOrTeacherSelected);
            }
        });

        goToCreateExamButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                updateDisplayItems();
                teacherController.setVisible(false);
                createExam.setVisible(true);

            }
        });

        goToCheckStudentGradesButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                //get list of students and add to like below TODO
                updateDisplayItems();
                teacherController.setVisible(false);
                CheckStudentExams.setVisible(true);

            }
        });

        checkGradesButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {

                String studentName = comboBox1.getSelectedItem().toString();
                CheckStudentExams.setVisible(false);

                //TODO GET STUDENT GRADES from studentName, exact sames as student self report of grades
                textArea2.setText("RETURN FROM Function");
                TeacherGradeView.setVisible(true);
            }
        });

        goBackToDashboardButton1.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                TeacherGradeView.setVisible(false);
                teacherController.setVisible(true);
            }
        });

        createExamBackToTeacherAdmin.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {

                String classTakingExam = classTakingBox.getSelectedItem().toString();
                String examTitle = examTitleTextField.getText();
                Course classTaking = getCourseFromName(classTakingExam);
                Exam e = new Exam(0, classTaking.courseID, examTitle);
                insertExam(e);
                Exam et = getExamWithName(examTitle);

                String question = questionTextField.getText();
                String answer = answerTextField.getText();
                Question q = new Question(question, answer);
                q.examID = et.examID;
                addQuestionForExam(q);

                String question2 = question2TextField.getText();
                String answer2 = answer2TextField.getText();
                q = new Question(question2, answer2);
                q.examID = et.examID;
                addQuestionForExam(q);

                String question3 = question3TextField.getText();
                String answer3 = answer3TextField.getText();
                q = new Question(question3, answer3);
                q.examID = et.examID;
                addQuestionForExam(q);

                String question4 = question4TextField.getText();
                String answer4 = answer4TextField.getText();
                q = new Question(question4, answer4);
                q.examID = et.examID;
                addQuestionForExam(q);

                String question5 = question5TextField.getText();
                String answer5 = answer5TextField.getText();
                q = new Question(question5, answer5);
                q.examID = et.examID;
                addQuestionForExam(q);
                createExam.setVisible(false);
                teacherController.setVisible(true);
            }
        });

///ALL BELOW FOR ADMIN ~~~~~~~~~~
        goToCreateStudentButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                adminController.setVisible(false);
                createStudentInput.setVisible(true);

            }
        });


        goToCreateClassButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                adminController.setVisible(false);
                createClassInput.setVisible(true);
            }
        });

        goToCreateStudentButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                adminController.setVisible(false);
                createStudentInput.setVisible(true);
            }
        });


        goToCreateTeacherButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                adminController.setVisible(false);
                createTeacherInput.setVisible(true);

            }
        });


        backToAdminControllerButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                createStudentInput.setVisible(false);
                adminController.setVisible(true);
            }
        });
        backToAdminControllerButton1.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                createTeacherInput.setVisible(false);
                //add all the other create Pages

                adminController.setVisible(true);
            }
        });
        backToAdminControllerButton2.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                createClassInput.setVisible(false);
                //add all the other create Pages

                adminController.setVisible(true);
            }
        });

        studentNameTextField.addKeyListener(new KeyListener() {
            @Override
            public void keyTyped(KeyEvent keyEvent) {

            }

            @Override
            public void keyPressed(KeyEvent keyEvent) {
                if (keyEvent.getKeyCode() == KeyEvent.VK_ENTER) {
                    String studentName = studentNameTextField.getText();
                    String studentPassword = PasswordForStudentTextField.getText();
                    System.out.print(studentName);
                    createUser(studentName, studentPassword, "Student");
                    createStudentInput.setVisible(false);
                    adminController.setVisible(true);
                    updateDisplayItems();
                }
            }

            @Override
            public void keyReleased(KeyEvent keyEvent) {

            }
        });

        teacherNameTextField.addKeyListener(new KeyListener() {
            @Override
            public void keyTyped(KeyEvent keyEvent) {

            }

            @Override
            public void keyPressed(KeyEvent keyEvent) {
                if (keyEvent.getKeyCode() == KeyEvent.VK_ENTER) {
                    String teacherName = teacherNameTextField.getText();
                    String teacherPassword = PasswordForTeacherTextField.getText();
                    System.out.print(teacherName);
                    createUser(teacherName, teacherPassword, "Teacher");
                    createTeacherInput.setVisible(false);
                    adminController.setVisible(true);
                    updateDisplayItems();
                }
            }

            @Override
            public void keyReleased(KeyEvent keyEvent) {

            }
        });

        classNameTextField.addKeyListener(new KeyListener() {
            @Override
            public void keyTyped(KeyEvent keyEvent) {

            }

            @Override
            public void keyPressed(KeyEvent keyEvent) {
                if (keyEvent.getKeyCode() == KeyEvent.VK_ENTER) {
                    String className = classNameTextField.getText();
                    System.out.print(className);
                    createCourse(className);
                    createClassInput.setVisible(false);
                    adminController.setVisible(true);
                    updateDisplayItems();
                }
            }

            @Override
            public void keyReleased(KeyEvent keyEvent) {

            }
        });

        testButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                Boolean temp = TestQuery(10, "garrettlord", "crazypassword", "teac");
            }
        });
//DONE ADMIN ~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        setVisible(true);
        backToLoginButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                adminController.setVisible(false);
                loginPanel.setVisible(true);
            }
        });


        button2.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                //TODO get EXAM SCORES VERBOSE TO STRING AND PRINTING
                textArea1.setText("BLAH GRADES BLAH BLAAH");
            }
        });
        goBackToDashboardButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                studentResultGrades.setVisible(false);
                StudentsController.setVisible(true);
            }
        });
        checkExamScoresButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                StudentsController.setVisible(false);
                studentResultGrades.setVisible(true);
            }
        });

        logoutButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                teacherController.setVisible(false);
                loginPanel.setVisible(true);
            }
        });
        logOutButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                StudentsController.setVisible(false);
                loginPanel.setVisible(true);
            }
        });
    }

    public void updateDisplayItems() {
        comboBox1.removeAllItems();
        comboBox2.removeAllItems();
        studentToClassBox2.removeAllItems();
        classSelected.removeAllItems();
        classTakingBox.removeAllItems();


        List<Student> std = getStudents();
        for (Student s : std) {
            studentToClassBox2.addItem(s.username);
            comboBox1.addItem(s.username);
        }

        List<Teacher> tch = getTeachers();
        for (Teacher t : tch) {
            studentToClassBox2.addItem(t.username);
            comboBox1.addItem(t.username);
        }

        List<Course> crs = getCourses();
        for (Course c : crs) {
            classSelected.addItem(c.name);
            classTakingBox.addItem(c.name);
        }

        if (currentUser.type.equals("Student")) {
            List<Exam> exms = getPossibleExamsForStudent((Student) currentUser);
            for (Exam e : exms) {
                comboBox2.addItem(e._name);
            }
        }
    }

    public boolean login(String _user, String _pass) {
        try {
            PreparedStatement stmt = con.prepareStatement("select * from users where username=? and pass=?");
            stmt.setString(1, _user);
            stmt.setString(2, _pass);
            ResultSet set = stmt.executeQuery();

            int userId = 0;
            String name = "";
            String type = "";
            if (set.next()) {
                userId = set.getInt(1);
                name = set.getString(2);
                type = set.getString(4);
            }

            if (type.equals("Administrator")) {
                System.out.println("Admin Logged In");
                currentUser = new Administrator(userId, name);
            } else if (type.equals("Teacher")) {
                System.out.println("Teacher Logged In");
                currentUser = new Teacher(userId, name);
            } else if (type.equals("Student")) {
                System.out.println("Student Logged In");
                currentUser = new Student(userId, name);
            } else {
                System.out.println("Failed to find any with that name");
            }

            System.out.println(set.getFetchSize());
            return set.getFetchSize() == 1;
        } catch (Exception e) {
            System.out.println("Failed");
            e.printStackTrace();
            for (int i = 0; i < 20; i++) {
                StackTraceElement str = e.getStackTrace()[i];
            }
            return false;
        }
    }

    public boolean createUser(String name, String pass, String type) {
        if (type.equals("Teacher")) {
            Teacher t = new Teacher();
            t.username = name;
            insertTeacher(t, pass);
            return true;
        } else if (type.equals("Student")) {
            Student s = new Student();
            s.username = name;
            insertStudent(s, pass);
            return true;
        }
        return false;
    }

    public void createCourse(String name) {
        Course c = new Course(0, 0, name);
        insertCourse(c);
    }

    public void createExam(String name, Course c) {
        Exam e = new Exam(0, c.courseID, name);
        insertExam(e);
    }

    public void addQuestionForExam(Question q) {
        insertQuestion(q);
    }

    public List<Question> getQuestionsForExam(Exam e) {
        try {
            System.out.println("" + e.examID);
            PreparedStatement stmt = con.prepareStatement("select * from questions where examID=" + e.examID);
            ResultSet set = stmt.executeQuery();
            ArrayList<Question> ret = new ArrayList<Question>();

            while (set.next()) {
                int qId = set.getInt("questionID");
                int eId = set.getInt("examID");
                String ques = set.getString("question");
                String answ = set.getString("answer");
                Question q = new Question(ques, answ);
                q.questionID = qId;
                q.examID = eId;
                ret.add(q);
            }

            return ret;
        } catch (Exception ex) {
            System.out.println("Failed");
            ex.printStackTrace();
            for (int i = 0; i < 20; i++) {
                StackTraceElement str = ex.getStackTrace()[i];
            }
            return null;
        }
    }

    public void assignStudentExam(Student s, Exam e) {
        String stmt = "INSERT INTO student_exams VALUES(" + s.userID + ", " + e.examID + ");";
        ArrayList<String> attrs = new ArrayList<String>();
        insertObject(stmt, attrs);
    }


    public void updateQuestion(Question q) {
        String str = "update questions set answer=" + q._answer + " where questionID=" + q.questionID;
        PreparedStatement stmt = null;
        try {
            stmt = con.prepareStatement(str);
            stmt.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void assignCourseToUser(String c, String u) {
        User std = getUserForUsername(u);
        Course crs = getCourseFromName(c);

        String str;
        ArrayList<String> attrs = new ArrayList<String>();
        if (std.type.equals("Student")) {
            str = "INSERT INTO registrations VALUES(" + std.userID + ", " + crs.courseID + ");";
            insertObject(str, attrs);
        } else {


            str = "update courses set teacherID=" + std.userID + " where courseID=" + crs.courseID;
            PreparedStatement stmt = null;
            try {
                stmt = con.prepareStatement(str);
                stmt.executeUpdate();
            } catch (SQLException e) {
                e.printStackTrace();
            }

        }


    }

    private ExamResponse getEResponseByExam(Exam e) {
        try {
            PreparedStatement stmt = con.prepareStatement("select * from exam_responses where examID=" + e.examID);
            ResultSet set = stmt.executeQuery();

            ExamResponse s = null;
            if (set.next()) {
                int exRId = set.getInt("exam_responseID");
                int stdId = set.getInt("examID");
                int usdId = set.getInt("userID");
                double score = set.getDouble("score");
                boolean sub = set.getBoolean("submitted");
                s = new ExamResponse(0, 0);
                s.examResponseID = exRId;
                s.examID = stdId;
                s.userID = usdId;
                s.score = score;
                s.submitted = sub;
            }
            return s;
        } catch (Exception er) {
            System.out.println("Failed");
            er.printStackTrace();
            for (int i = 0; i < 20; i++) {
                StackTraceElement str = er.getStackTrace()[i];
            }
            return null;
        }
    }

    private Exam getExamWithName(String name) {
        try {
            PreparedStatement stmt = con.prepareStatement("select * from exams where name=?");
            stmt.setString(1, name);
            ResultSet set = stmt.executeQuery();

            Exam s = null;
            if (set.next()) {
                int stdId = set.getInt("examID");
                int cId = set.getInt("courseID");
                String user = set.getString("name");
                s = new Exam(stdId, cId, user);
            }
            return s;
        } catch (Exception e) {
            System.out.println("Failed");
            e.printStackTrace();
            for (int i = 0; i < 20; i++) {
                StackTraceElement str = e.getStackTrace()[i];
            }
            return null;
        }
    }

    private User getUserForUsername(String username) {
        try {
            PreparedStatement stmt = con.prepareStatement("select * from users where username=?");
            stmt.setString(1, username);
            ResultSet set = stmt.executeQuery();

            User s = null;
            if (set.next()) {
                int stdId = set.getInt("userID");
                String user = set.getString("username");
                String type = set.getString("type");
                s = new User();
                s.userID = stdId;
                s.username = user;
                s.type = type;
            }

            return s;
        } catch (Exception e) {
            System.out.println("Failed");
            e.printStackTrace();
            for (int i = 0; i < 20; i++) {
                StackTraceElement str = e.getStackTrace()[i];
            }
            return null;
        }
    }

    private Course getCourseFromName(String name) {
        try {
            PreparedStatement stmt = con.prepareStatement("select * from courses where name=?");
            stmt.setString(1, name);
            ResultSet set = stmt.executeQuery();

            Course c = null;
            if (set.next()) {
                int stdId = set.getInt("courseID");
                int tchId = set.getInt("teacherID");
                String user = set.getString("name");
                c = new Course(stdId, tchId, user);
            }

            return c;
        } catch (Exception e) {
            System.out.println("Failed");
            e.printStackTrace();
            for (int i = 0; i < 20; i++) {
                StackTraceElement str = e.getStackTrace()[i];
            }
            return null;
        }
    }


    public List<String> getGradesForStudent(Student s) {
        List<ExamResponse> sExams = getExamsForStudent(s);
        ArrayList<String> ret = new ArrayList<String>();
        for (ExamResponse e : sExams) {
            ret.add("" + e.score);
        }
        return ret;
    }

    public List<Course> getCourses() {
        try {
            PreparedStatement stmt = con.prepareStatement("select * from courses");
            ResultSet set = stmt.executeQuery();
            ArrayList<Course> ret = new ArrayList<Course>();

            while (set.next()) {
                int courseId = set.getInt("courseID");
                int teacherId = set.getInt("teacherID");
                String name = set.getString("name");
                Course c = new Course(courseId, teacherId, name);
                ret.add(c);
            }

            return ret;
        } catch (Exception e) {
            System.out.println("Failed");
            e.printStackTrace();
            for (int i = 0; i < 20; i++) {
                StackTraceElement str = e.getStackTrace()[i];
            }
            return null;
        }
    }

    public List<Exam> getPossibleExamsForStudent(Student s) {
        List<Course> crs = getCoursesForStudent(s);
        ArrayList<Exam> ret = new ArrayList<Exam>();
        for (Course c : crs) {
            ArrayList<Exam> ex = (ArrayList<Exam>) getExamsForCourse(c);
            for (Exam e : ex) {
                ret.add(e);
            }
        }
        return ret;
    }

    public List<Course> getCoursesForStudent(Student s) {
        try {
            PreparedStatement stmt = con.prepareStatement("select * from registrations where userID=" + s.userID);
            ResultSet set = stmt.executeQuery();

            ArrayList<String> strID = new ArrayList<String>();
            ArrayList<Course> ret = new ArrayList<Course>();

            while (set.next()) {
                strID.add("" + set.getInt("courseID"));
            }

            ArrayList<Course> crs = (ArrayList<Course>) getCourses();
            for (Course c : crs) {
                for (String id : strID) {
                    if ((c.courseID + "").equals(id)) {
                        ret.add(c);
                    }
                }
            }

            return ret;
        } catch (Exception e) {
            System.out.println("Failed");
            e.printStackTrace();
            for (int i = 0; i < 20; i++) {
                StackTraceElement str = e.getStackTrace()[i];
            }
            return null;
        }
    }

    public List<Exam> getExamsForCourse(Course c) {
        try {
            PreparedStatement stmt = con.prepareStatement("select * from exams where courseID=" + c.courseID);
            ResultSet set = stmt.executeQuery();
            ArrayList<Exam> ret = new ArrayList<Exam>();

            while (set.next()) {
                int courseId = set.getInt("examID");
                int teacherId = set.getInt("courseID");
                String name = set.getString("name");
                Exam e = new Exam(courseId, teacherId, name);
                ret.add(e);
            }

            return ret;
        } catch (Exception e) {
            System.out.println("Failed");
            e.printStackTrace();
            for (int i = 0; i < 20; i++) {
                StackTraceElement str = e.getStackTrace()[i];
            }
            return null;
        }
    }

    public List<Teacher> getTeachers() {
        ArrayList<Teacher> ret = new ArrayList<Teacher>();
        ArrayList<User> temp = (ArrayList<User>) getUserObjects("Teacher");
        for (User obj : temp) {
            ret.add((Teacher) obj);
        }
        return ret;
    }

    public List<Student> getStudents() {
        ArrayList<Student> ret = new ArrayList<Student>();
        ArrayList<User> temp = (ArrayList<User>) getUserObjects("Student");
        for (User obj : temp) {
            ret.add((Student) obj);
        }
        return ret;
    }

    private List<User> getUserObjects(String type) {
        try {
            PreparedStatement stmt = con.prepareStatement("select * from users where type=?");
            stmt.setString(1, type);
            ResultSet set = stmt.executeQuery();
            ArrayList<User> ret = new ArrayList<User>();

            int userId = 0;
            String name = "";
            String edge = "";
            while (set.next()) {
                userId = set.getInt(1);
                name = set.getString(2);
                edge = set.getString(4);

                if (edge.equals("Teacher")) {
                    Teacher t = new Teacher(userId, name);
                    ret.add((User) t);
                } else if (edge.equals("Student")) {
                    Student s = new Student(userId, name);
                    ret.add((User) s);
                } else {
                    System.out.println("Failed to find any with that name");
                }
            }

            return ret;
        } catch (Exception e) {
            System.out.println("Failed");
            e.printStackTrace();
            for (int i = 0; i < 20; i++) {
                StackTraceElement str = e.getStackTrace()[i];
            }
            return null;
        }
    }

    public List<ExamResponse> getExamsForStudent(Student s) {
        try {
            PreparedStatement stmt = con.prepareStatement("select * from exam_responses where userID=" + s.userID);
            ResultSet set = stmt.executeQuery();
            ArrayList<ExamResponse> ret = new ArrayList<ExamResponse>();

            int examRespId = 0;
            double name = 0.0;
            while (set.next()) {
                examRespId = set.getInt("exam_responseID");
                name = set.getDouble("score");
                ExamResponse c = new ExamResponse(examRespId, name);
                ret.add(c);
            }

            return ret;
        } catch (Exception e) {
            System.out.println("Failed");
            e.printStackTrace();
            for (int i = 0; i < 20; i++) {
                StackTraceElement str = e.getStackTrace()[i];
            }
            return null;
        }
    }

    public void insertQResponse(QuestionResponse q) {
        String stmt = "INSERT INTO question_responses VALUES( DEFAULT, " + q.questionID + ", " + q.examResponseID + ", ?, " + q.correct + ");";
        ArrayList<String> attrs = new ArrayList<String>();
        attrs.add(q.answer);
        insertObject(stmt, attrs);
    }

    public void insertEResponse(ExamResponse e) {
        String stmt = "INSERT INTO exam_responses VALUES( DEFAULT, " + e.examID + ", " + e.userID + ", " + e.score + ", " + e.submitted + ");";
        ArrayList<String> attrs = new ArrayList<String>();
        insertObject(stmt, attrs);
    }

    public void insertTeacher(Teacher t, String pass) {
        String stmt = "INSERT INTO users VALUES( DEFAULT, ?, ?, ?);";
        ArrayList<String> attrs = new ArrayList<String>();
        attrs.add(t.username);
        attrs.add(pass);
        attrs.add(t.type);
        insertObject(stmt, attrs);
    }

    public void insertStudent(Student s, String pass) {
        String stmt = "INSERT INTO users VALUES( DEFAULT, ?, ?, ?);";
        ArrayList<String> attrs = new ArrayList<String>();
        attrs.add(s.username);
        attrs.add(pass);
        attrs.add(s.type);
        insertObject(stmt, attrs);
    }

    public void insertCourse(Course c) {
        String stmt = "INSERT INTO courses VALUES( DEFAULT, " + c.teacherID + ", ?);";
        ArrayList<String> attrs = new ArrayList<String>();
        attrs.add(c.name);
        insertObject(stmt, attrs);
    }

    public void insertExam(Exam e) {
        String stmt = "INSERT INTO exams VALUES( DEFAULT, " + e.courseID + ", ?);";
        ArrayList<String> attrs = new ArrayList<String>();
        attrs.add(e._name);
        insertObject(stmt, attrs);
    }


    public void insertQuestion(Question q) {
        String stmt = "INSERT INTO questions VALUES( DEFAULT, " + q.examID + ", ?, ? );";
        ArrayList<String> attrs = new ArrayList<String>();
        attrs.add(q._text);
        attrs.add(q._answer);
        insertObject(stmt, attrs);
    }

    private void insertObject(String s, List<String> attrs) {
        try {
            PreparedStatement stmt = con.prepareStatement(s);
            for (int i = 1; i <= attrs.size(); i++) {
                stmt.setString(i, attrs.get(i - 1));
            }

            int res = stmt.executeUpdate();
//            return res == 1;
        } catch (Exception e) {
            System.out.println("Failed");
            e.printStackTrace();
//            return false;
        }
    }

    /*
     *TEST DB ACCESS WALKTHROUGH
     */
    public boolean TestQuery(Integer _autoID, String _userName, String _password, String _userType) {
        try {
            PreparedStatement stmt = con.prepareStatement("INSERT INTO users VALUES(" + _autoID + ", ?, ?, ?);");
            stmt.setString(1, _userName);
            stmt.setString(2, _password);
            stmt.setString(3, _userType);
            int res = stmt.executeUpdate();
            return res == 1;
        } catch (Exception e) {
            System.out.println("Failed");
            e.printStackTrace();
            return false;
        }
    }

    /*
    * Connect to mysql database with grader account
    */
    public Connection connect() throws Exception {
        Class.forName("com.mysql.jdbc.Driver");

        // Set properties
        Connection conn = null;
        Properties connectionProps = new Properties();
        connectionProps.put("user", "gklord");
        connectionProps.put("password", "Password!");

        // establish connection
        conn = DriverManager.getConnection("jdbc:mysql://orion.csl.mtu.edu/gklord", connectionProps);
        return conn;
    }

    /*
     * Close connection to database
     */
    public void close() {
        try {
            con.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void createUIComponents() {
        // TODO: place custom component creation code here
    }

    {
// GUI initializer generated by IntelliJ IDEA GUI Designer
// >>> IMPORTANT!! <<<
// DO NOT EDIT OR ADD ANY CODE HERE!
        $$$setupUI$$$();
    }

    /**
     * Method generated by IntelliJ IDEA GUI Designer
     * >>> IMPORTANT!! <<<
     * DO NOT edit this method OR call it in your code!
     *
     * @noinspection ALL
     */
    private void $$$setupUI$$$() {
        rootPanel = new JPanel();
        rootPanel.setLayout(new com.intellij.uiDesigner.core.GridLayoutManager(16, 1, new Insets(10, 10, 10, 10), -1, -1));
        rootPanel.setBackground(new Color(-12894914));
        rootPanel.setMaximumSize(new Dimension(761, 475));
        rootPanel.setBorder(BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(), "ExamMaker", TitledBorder.DEFAULT_JUSTIFICATION, TitledBorder.DEFAULT_POSITION, new Font("Helvetica Neue", Font.PLAIN, rootPanel.getFont().getSize()), new Color(-1)));
        loginPanel = new JPanel();
        loginPanel.setLayout(new com.intellij.uiDesigner.core.GridLayoutManager(3, 1, new Insets(0, 0, 0, 0), -1, -1));
        rootPanel.add(loginPanel, new com.intellij.uiDesigner.core.GridConstraints(0, 0, 1, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_CENTER, com.intellij.uiDesigner.core.GridConstraints.FILL_BOTH, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_SHRINK | com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_GROW, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_SHRINK | com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_GROW, new Dimension(720, 400), new Dimension(720, 400), new Dimension(765, 460), 0, false));
        usernameTextField = new JTextField();
        usernameTextField.setText("Username");
        loginPanel.add(usernameTextField, new com.intellij.uiDesigner.core.GridConstraints(0, 0, 1, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_WEST, com.intellij.uiDesigner.core.GridConstraints.FILL_HORIZONTAL, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_WANT_GROW, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, null, new Dimension(150, -1), null, 0, false));
        loginButton = new JButton();
        loginButton.setEnabled(true);
        loginButton.setText("Login");
        loginPanel.add(loginButton, new com.intellij.uiDesigner.core.GridConstraints(2, 0, 1, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_CENTER, com.intellij.uiDesigner.core.GridConstraints.FILL_HORIZONTAL, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_SHRINK | com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_GROW, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        passwordTextField = new JPasswordField();
        passwordTextField.setText("Password");
        loginPanel.add(passwordTextField, new com.intellij.uiDesigner.core.GridConstraints(1, 0, 1, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_WEST, com.intellij.uiDesigner.core.GridConstraints.FILL_HORIZONTAL, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_WANT_GROW, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, null, new Dimension(150, -1), null, 0, false));
        debugPanel = new JPanel();
        debugPanel.setLayout(new com.intellij.uiDesigner.core.GridLayoutManager(3, 1, new Insets(0, 0, 0, 0), -1, -1));
        debugPanel.setEnabled(false);
        rootPanel.add(debugPanel, new com.intellij.uiDesigner.core.GridConstraints(1, 0, 1, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_CENTER, com.intellij.uiDesigner.core.GridConstraints.FILL_BOTH, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_SHRINK | com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_GROW, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_SHRINK | com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_GROW, null, null, null, 0, false));
        testButton = new JButton();
        testButton.setText("Test");
        debugPanel.add(testButton, new com.intellij.uiDesigner.core.GridConstraints(2, 0, 1, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_CENTER, com.intellij.uiDesigner.core.GridConstraints.FILL_HORIZONTAL, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_SHRINK | com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_GROW, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        button1 = new JButton();
        button1.setText("Button");
        debugPanel.add(button1, new com.intellij.uiDesigner.core.GridConstraints(1, 0, 1, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_CENTER, com.intellij.uiDesigner.core.GridConstraints.FILL_HORIZONTAL, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_SHRINK | com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_GROW, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        adminController = new JPanel();
        adminController.setLayout(new com.intellij.uiDesigner.core.GridLayoutManager(5, 4, new Insets(0, 0, 0, 0), -1, -1));
        rootPanel.add(adminController, new com.intellij.uiDesigner.core.GridConstraints(2, 0, 1, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_CENTER, com.intellij.uiDesigner.core.GridConstraints.FILL_BOTH, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_SHRINK | com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_GROW, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_SHRINK | com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_GROW, null, null, null, 0, false));
        final com.intellij.uiDesigner.core.Spacer spacer1 = new com.intellij.uiDesigner.core.Spacer();
        adminController.add(spacer1, new com.intellij.uiDesigner.core.GridConstraints(0, 1, 1, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_CENTER, com.intellij.uiDesigner.core.GridConstraints.FILL_HORIZONTAL, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_WANT_GROW, 1, null, null, null, 0, false));
        final com.intellij.uiDesigner.core.Spacer spacer2 = new com.intellij.uiDesigner.core.Spacer();
        adminController.add(spacer2, new com.intellij.uiDesigner.core.GridConstraints(1, 0, 3, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_CENTER, com.intellij.uiDesigner.core.GridConstraints.FILL_VERTICAL, 1, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_WANT_GROW, null, null, null, 0, false));
        goToCreateClassButton = new JButton();
        goToCreateClassButton.setText("Go to Create Class");
        adminController.add(goToCreateClassButton, new com.intellij.uiDesigner.core.GridConstraints(2, 1, 1, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_CENTER, com.intellij.uiDesigner.core.GridConstraints.FILL_HORIZONTAL, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_SHRINK | com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_GROW, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        backToLoginButton = new JButton();
        backToLoginButton.setText("Back to Login");
        adminController.add(backToLoginButton, new com.intellij.uiDesigner.core.GridConstraints(4, 1, 1, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_CENTER, com.intellij.uiDesigner.core.GridConstraints.FILL_HORIZONTAL, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_SHRINK | com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_GROW, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        goToCreateTeacherButton = new JButton();
        goToCreateTeacherButton.setText("Go to Create Teacher");
        adminController.add(goToCreateTeacherButton, new com.intellij.uiDesigner.core.GridConstraints(2, 3, 1, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_CENTER, com.intellij.uiDesigner.core.GridConstraints.FILL_HORIZONTAL, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_SHRINK | com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_GROW, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        goToCreateStudentButton = new JButton();
        goToCreateStudentButton.setText("Go to Create Student");
        adminController.add(goToCreateStudentButton, new com.intellij.uiDesigner.core.GridConstraints(2, 2, 1, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_CENTER, com.intellij.uiDesigner.core.GridConstraints.FILL_HORIZONTAL, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_SHRINK | com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_GROW, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        classSelected = new JComboBox();
        final DefaultComboBoxModel defaultComboBoxModel1 = new DefaultComboBoxModel();
        classSelected.setModel(defaultComboBoxModel1);
        adminController.add(classSelected, new com.intellij.uiDesigner.core.GridConstraints(3, 3, 1, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_WEST, com.intellij.uiDesigner.core.GridConstraints.FILL_HORIZONTAL, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_GROW, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        addTeacherOrStudentButton = new JButton();
        addTeacherOrStudentButton.setText("Add Teacher or Student to Class");
        adminController.add(addTeacherOrStudentButton, new com.intellij.uiDesigner.core.GridConstraints(3, 1, 1, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_CENTER, com.intellij.uiDesigner.core.GridConstraints.FILL_HORIZONTAL, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_SHRINK | com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_GROW, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        studentToClassBox2 = new JComboBox();
        adminController.add(studentToClassBox2, new com.intellij.uiDesigner.core.GridConstraints(3, 2, 1, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_WEST, com.intellij.uiDesigner.core.GridConstraints.FILL_HORIZONTAL, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_GROW, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        createStudentInput = new JPanel();
        createStudentInput.setLayout(new com.intellij.uiDesigner.core.GridLayoutManager(3, 1, new Insets(0, 0, 0, 0), -1, -1));
        rootPanel.add(createStudentInput, new com.intellij.uiDesigner.core.GridConstraints(3, 0, 1, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_CENTER, com.intellij.uiDesigner.core.GridConstraints.FILL_BOTH, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_SHRINK | com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_GROW, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_SHRINK | com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_GROW, null, null, null, 0, false));
        studentNameTextField = new JTextField();
        studentNameTextField.setText("Please type student name and press enter");
        createStudentInput.add(studentNameTextField, new com.intellij.uiDesigner.core.GridConstraints(2, 0, 1, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_WEST, com.intellij.uiDesigner.core.GridConstraints.FILL_HORIZONTAL, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_WANT_GROW, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, null, new Dimension(150, -1), null, 0, false));
        backToAdminControllerButton = new JButton();
        backToAdminControllerButton.setText("Back to Admin Controller");
        createStudentInput.add(backToAdminControllerButton, new com.intellij.uiDesigner.core.GridConstraints(0, 0, 1, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_CENTER, com.intellij.uiDesigner.core.GridConstraints.FILL_HORIZONTAL, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_SHRINK | com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_GROW, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, null, null, null, 1, false));
        PasswordForStudentTextField = new JTextField();
        PasswordForStudentTextField.setText("Please enter password for student");
        createStudentInput.add(PasswordForStudentTextField, new com.intellij.uiDesigner.core.GridConstraints(1, 0, 1, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_WEST, com.intellij.uiDesigner.core.GridConstraints.FILL_HORIZONTAL, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_WANT_GROW, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, null, new Dimension(150, -1), null, 0, false));
        createTeacherInput = new JPanel();
        createTeacherInput.setLayout(new com.intellij.uiDesigner.core.GridLayoutManager(3, 2, new Insets(0, 0, 0, 0), -1, -1));
        rootPanel.add(createTeacherInput, new com.intellij.uiDesigner.core.GridConstraints(4, 0, 1, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_CENTER, com.intellij.uiDesigner.core.GridConstraints.FILL_BOTH, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_SHRINK | com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_GROW, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_SHRINK | com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_GROW, null, null, null, 0, false));
        teacherNameTextField = new JTextField();
        teacherNameTextField.setText("Please type teacher name and press enter");
        createTeacherInput.add(teacherNameTextField, new com.intellij.uiDesigner.core.GridConstraints(2, 0, 1, 2, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_WEST, com.intellij.uiDesigner.core.GridConstraints.FILL_HORIZONTAL, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_WANT_GROW, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, null, new Dimension(150, -1), null, 0, false));
        final com.intellij.uiDesigner.core.Spacer spacer3 = new com.intellij.uiDesigner.core.Spacer();
        createTeacherInput.add(spacer3, new com.intellij.uiDesigner.core.GridConstraints(0, 1, 1, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_CENTER, com.intellij.uiDesigner.core.GridConstraints.FILL_VERTICAL, 1, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_WANT_GROW, null, null, null, 0, false));
        backToAdminControllerButton1 = new JButton();
        backToAdminControllerButton1.setText("Back To Admin Controller");
        createTeacherInput.add(backToAdminControllerButton1, new com.intellij.uiDesigner.core.GridConstraints(0, 0, 1, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_CENTER, com.intellij.uiDesigner.core.GridConstraints.FILL_HORIZONTAL, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_SHRINK | com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_GROW, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        PasswordForTeacherTextField = new JTextField();
        PasswordForTeacherTextField.setText("Please enter password for teacher");
        createTeacherInput.add(PasswordForTeacherTextField, new com.intellij.uiDesigner.core.GridConstraints(1, 0, 1, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_WEST, com.intellij.uiDesigner.core.GridConstraints.FILL_HORIZONTAL, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_WANT_GROW, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, null, new Dimension(150, -1), null, 0, false));
        createClassInput = new JPanel();
        createClassInput.setLayout(new com.intellij.uiDesigner.core.GridLayoutManager(2, 1, new Insets(0, 0, 0, 0), -1, -1));
        rootPanel.add(createClassInput, new com.intellij.uiDesigner.core.GridConstraints(5, 0, 1, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_CENTER, com.intellij.uiDesigner.core.GridConstraints.FILL_BOTH, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_SHRINK | com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_GROW, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_SHRINK | com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_GROW, null, null, null, 0, false));
        classNameTextField = new JTextField();
        classNameTextField.setText("Please type class name and press enter");
        createClassInput.add(classNameTextField, new com.intellij.uiDesigner.core.GridConstraints(1, 0, 1, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_WEST, com.intellij.uiDesigner.core.GridConstraints.FILL_HORIZONTAL, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_WANT_GROW, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, null, new Dimension(150, -1), null, 0, false));
        backToAdminControllerButton2 = new JButton();
        backToAdminControllerButton2.setText("Back To Admin Controller");
        createClassInput.add(backToAdminControllerButton2, new com.intellij.uiDesigner.core.GridConstraints(0, 0, 1, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_CENTER, com.intellij.uiDesigner.core.GridConstraints.FILL_HORIZONTAL, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_SHRINK | com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_GROW, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        teacherController = new JPanel();
        teacherController.setLayout(new com.intellij.uiDesigner.core.GridLayoutManager(4, 1, new Insets(0, 0, 0, 0), -1, -1));
        rootPanel.add(teacherController, new com.intellij.uiDesigner.core.GridConstraints(6, 0, 1, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_CENTER, com.intellij.uiDesigner.core.GridConstraints.FILL_BOTH, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_SHRINK | com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_GROW, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_SHRINK | com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_GROW, null, null, null, 0, false));
        goToCheckStudentGradesButton = new JButton();
        goToCheckStudentGradesButton.setText("Go to check student grades");
        teacherController.add(goToCheckStudentGradesButton, new com.intellij.uiDesigner.core.GridConstraints(2, 0, 1, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_CENTER, com.intellij.uiDesigner.core.GridConstraints.FILL_HORIZONTAL, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_SHRINK | com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_GROW, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        final com.intellij.uiDesigner.core.Spacer spacer4 = new com.intellij.uiDesigner.core.Spacer();
        teacherController.add(spacer4, new com.intellij.uiDesigner.core.GridConstraints(0, 0, 1, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_CENTER, com.intellij.uiDesigner.core.GridConstraints.FILL_VERTICAL, 1, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_WANT_GROW, null, null, null, 0, false));
        goToCreateExamButton = new JButton();
        goToCreateExamButton.setText("Go to create exam");
        teacherController.add(goToCreateExamButton, new com.intellij.uiDesigner.core.GridConstraints(1, 0, 1, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_CENTER, com.intellij.uiDesigner.core.GridConstraints.FILL_HORIZONTAL, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_SHRINK | com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_GROW, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        logoutButton = new JButton();
        logoutButton.setText("Logout");
        teacherController.add(logoutButton, new com.intellij.uiDesigner.core.GridConstraints(3, 0, 1, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_CENTER, com.intellij.uiDesigner.core.GridConstraints.FILL_HORIZONTAL, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_SHRINK | com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_GROW, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        createExam = new JPanel();
        createExam.setLayout(new com.intellij.uiDesigner.core.GridLayoutManager(8, 5, new Insets(0, 0, 0, 0), -1, -1));
        rootPanel.add(createExam, new com.intellij.uiDesigner.core.GridConstraints(7, 0, 1, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_CENTER, com.intellij.uiDesigner.core.GridConstraints.FILL_BOTH, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_SHRINK | com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_GROW, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_SHRINK | com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_GROW, null, null, null, 0, false));
        examTitleTextField = new JTextField();
        examTitleTextField.setText("Exam Title");
        createExam.add(examTitleTextField, new com.intellij.uiDesigner.core.GridConstraints(1, 0, 1, 5, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_WEST, com.intellij.uiDesigner.core.GridConstraints.FILL_HORIZONTAL, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_WANT_GROW, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, null, new Dimension(150, -1), null, 0, false));
        final com.intellij.uiDesigner.core.Spacer spacer5 = new com.intellij.uiDesigner.core.Spacer();
        createExam.add(spacer5, new com.intellij.uiDesigner.core.GridConstraints(2, 4, 1, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_CENTER, com.intellij.uiDesigner.core.GridConstraints.FILL_VERTICAL, 1, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_WANT_GROW, null, null, null, 0, false));
        questionTextField = new JTextField();
        questionTextField.setText("Question 1");
        createExam.add(questionTextField, new com.intellij.uiDesigner.core.GridConstraints(2, 0, 1, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_WEST, com.intellij.uiDesigner.core.GridConstraints.FILL_HORIZONTAL, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_WANT_GROW, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, null, new Dimension(150, -1), null, 0, false));
        answerTextField = new JTextField();
        answerTextField.setText("Answer 1");
        createExam.add(answerTextField, new com.intellij.uiDesigner.core.GridConstraints(2, 1, 1, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_WEST, com.intellij.uiDesigner.core.GridConstraints.FILL_HORIZONTAL, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_WANT_GROW, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, null, new Dimension(150, -1), null, 0, false));
        createExamBackToTeacherAdmin = new JButton();
        createExamBackToTeacherAdmin.setText("Create Exam - Back to Teacher Admin");
        createExam.add(createExamBackToTeacherAdmin, new com.intellij.uiDesigner.core.GridConstraints(7, 1, 1, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_CENTER, com.intellij.uiDesigner.core.GridConstraints.FILL_HORIZONTAL, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_SHRINK | com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_GROW, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        question2TextField = new JTextField();
        question2TextField.setText("Question 2");
        createExam.add(question2TextField, new com.intellij.uiDesigner.core.GridConstraints(3, 0, 1, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_WEST, com.intellij.uiDesigner.core.GridConstraints.FILL_HORIZONTAL, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_WANT_GROW, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, null, new Dimension(150, -1), null, 0, false));
        answer2TextField = new JTextField();
        answer2TextField.setText("Answer 2");
        createExam.add(answer2TextField, new com.intellij.uiDesigner.core.GridConstraints(3, 1, 1, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_WEST, com.intellij.uiDesigner.core.GridConstraints.FILL_HORIZONTAL, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_WANT_GROW, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, null, new Dimension(150, -1), null, 0, false));
        question3TextField = new JTextField();
        question3TextField.setText("Question 3");
        createExam.add(question3TextField, new com.intellij.uiDesigner.core.GridConstraints(4, 0, 1, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_WEST, com.intellij.uiDesigner.core.GridConstraints.FILL_HORIZONTAL, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_WANT_GROW, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, null, new Dimension(150, -1), null, 0, false));
        answer3TextField = new JTextField();
        answer3TextField.setText("Answer 3");
        createExam.add(answer3TextField, new com.intellij.uiDesigner.core.GridConstraints(4, 1, 1, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_WEST, com.intellij.uiDesigner.core.GridConstraints.FILL_HORIZONTAL, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_WANT_GROW, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, null, new Dimension(150, -1), null, 0, false));
        question4TextField = new JTextField();
        question4TextField.setText("Question 4");
        createExam.add(question4TextField, new com.intellij.uiDesigner.core.GridConstraints(5, 0, 1, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_WEST, com.intellij.uiDesigner.core.GridConstraints.FILL_HORIZONTAL, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_WANT_GROW, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, null, new Dimension(150, -1), null, 0, false));
        answer4TextField = new JTextField();
        answer4TextField.setText("Answer 4");
        createExam.add(answer4TextField, new com.intellij.uiDesigner.core.GridConstraints(5, 1, 1, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_WEST, com.intellij.uiDesigner.core.GridConstraints.FILL_HORIZONTAL, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_WANT_GROW, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, null, new Dimension(150, -1), null, 0, false));
        question5TextField = new JTextField();
        question5TextField.setText("Question 5");
        createExam.add(question5TextField, new com.intellij.uiDesigner.core.GridConstraints(6, 0, 1, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_WEST, com.intellij.uiDesigner.core.GridConstraints.FILL_HORIZONTAL, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_WANT_GROW, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, null, new Dimension(150, -1), null, 0, false));
        answer5TextField = new JTextField();
        answer5TextField.setText("Answer 5");
        createExam.add(answer5TextField, new com.intellij.uiDesigner.core.GridConstraints(6, 1, 1, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_WEST, com.intellij.uiDesigner.core.GridConstraints.FILL_HORIZONTAL, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_WANT_GROW, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, null, new Dimension(150, -1), null, 0, false));
        classTakingBox = new JComboBox();
        createExam.add(classTakingBox, new com.intellij.uiDesigner.core.GridConstraints(0, 0, 1, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_WEST, com.intellij.uiDesigner.core.GridConstraints.FILL_HORIZONTAL, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_GROW, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        CheckStudentExams = new JPanel();
        CheckStudentExams.setLayout(new com.intellij.uiDesigner.core.GridLayoutManager(1, 2, new Insets(0, 0, 0, 0), -1, -1));
        rootPanel.add(CheckStudentExams, new com.intellij.uiDesigner.core.GridConstraints(8, 0, 1, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_CENTER, com.intellij.uiDesigner.core.GridConstraints.FILL_BOTH, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_SHRINK | com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_GROW, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_SHRINK | com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_GROW, null, null, null, 0, false));
        comboBox1 = new JComboBox();
        CheckStudentExams.add(comboBox1, new com.intellij.uiDesigner.core.GridConstraints(0, 1, 1, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_WEST, com.intellij.uiDesigner.core.GridConstraints.FILL_HORIZONTAL, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_GROW, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        checkGradesButton = new JButton();
        checkGradesButton.setText("Check grades");
        CheckStudentExams.add(checkGradesButton, new com.intellij.uiDesigner.core.GridConstraints(0, 0, 1, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_CENTER, com.intellij.uiDesigner.core.GridConstraints.FILL_HORIZONTAL, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_SHRINK | com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_GROW, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        addQuestionsToExam = new JPanel();
        addQuestionsToExam.setLayout(new com.intellij.uiDesigner.core.GridLayoutManager(1, 1, new Insets(0, 0, 0, 0), -1, -1));
        rootPanel.add(addQuestionsToExam, new com.intellij.uiDesigner.core.GridConstraints(9, 0, 1, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_CENTER, com.intellij.uiDesigner.core.GridConstraints.FILL_BOTH, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_SHRINK | com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_GROW, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_SHRINK | com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_GROW, null, null, null, 0, false));
        studentResultGrades = new JPanel();
        studentResultGrades.setLayout(new com.intellij.uiDesigner.core.GridLayoutManager(3, 3, new Insets(0, 0, 0, 0), -1, -1));
        rootPanel.add(studentResultGrades, new com.intellij.uiDesigner.core.GridConstraints(10, 0, 1, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_CENTER, com.intellij.uiDesigner.core.GridConstraints.FILL_BOTH, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_SHRINK | com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_GROW, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_SHRINK | com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_GROW, null, null, null, 0, false));
        final com.intellij.uiDesigner.core.Spacer spacer6 = new com.intellij.uiDesigner.core.Spacer();
        studentResultGrades.add(spacer6, new com.intellij.uiDesigner.core.GridConstraints(2, 0, 1, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_CENTER, com.intellij.uiDesigner.core.GridConstraints.FILL_VERTICAL, 1, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_WANT_GROW, null, null, null, 0, false));
        textArea1 = new JTextArea();
        studentResultGrades.add(textArea1, new com.intellij.uiDesigner.core.GridConstraints(1, 1, 1, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_CENTER, com.intellij.uiDesigner.core.GridConstraints.FILL_BOTH, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_WANT_GROW, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_WANT_GROW, null, new Dimension(150, 50), null, 0, false));
        button2 = new JButton();
        button2.setText("Check for latest grades");
        studentResultGrades.add(button2, new com.intellij.uiDesigner.core.GridConstraints(0, 1, 1, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_CENTER, com.intellij.uiDesigner.core.GridConstraints.FILL_HORIZONTAL, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_SHRINK | com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_GROW, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        goBackToDashboardButton = new JButton();
        goBackToDashboardButton.setText("Go back to dashboard");
        studentResultGrades.add(goBackToDashboardButton, new com.intellij.uiDesigner.core.GridConstraints(1, 2, 1, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_CENTER, com.intellij.uiDesigner.core.GridConstraints.FILL_HORIZONTAL, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_SHRINK | com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_GROW, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        StudentsController = new JPanel();
        StudentsController.setLayout(new com.intellij.uiDesigner.core.GridLayoutManager(2, 3, new Insets(0, 0, 0, 0), -1, -1));
        rootPanel.add(StudentsController, new com.intellij.uiDesigner.core.GridConstraints(11, 0, 1, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_CENTER, com.intellij.uiDesigner.core.GridConstraints.FILL_BOTH, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_SHRINK | com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_GROW, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_SHRINK | com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_GROW, null, null, null, 0, false));
        final com.intellij.uiDesigner.core.Spacer spacer7 = new com.intellij.uiDesigner.core.Spacer();
        StudentsController.add(spacer7, new com.intellij.uiDesigner.core.GridConstraints(1, 0, 1, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_CENTER, com.intellij.uiDesigner.core.GridConstraints.FILL_VERTICAL, 1, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_WANT_GROW, null, null, null, 0, false));
        checkExamScoresButton = new JButton();
        checkExamScoresButton.setText("Check Exam Scores");
        StudentsController.add(checkExamScoresButton, new com.intellij.uiDesigner.core.GridConstraints(1, 1, 1, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_CENTER, com.intellij.uiDesigner.core.GridConstraints.FILL_HORIZONTAL, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_SHRINK | com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_GROW, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        takeExamButton = new JButton();
        takeExamButton.setText("Take Exam");
        StudentsController.add(takeExamButton, new com.intellij.uiDesigner.core.GridConstraints(0, 2, 1, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_CENTER, com.intellij.uiDesigner.core.GridConstraints.FILL_HORIZONTAL, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_SHRINK | com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_GROW, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        comboBox2 = new JComboBox();
        StudentsController.add(comboBox2, new com.intellij.uiDesigner.core.GridConstraints(0, 1, 1, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_WEST, com.intellij.uiDesigner.core.GridConstraints.FILL_HORIZONTAL, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_GROW, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        logOutButton = new JButton();
        logOutButton.setText("Log Out");
        StudentsController.add(logOutButton, new com.intellij.uiDesigner.core.GridConstraints(1, 2, 1, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_CENTER, com.intellij.uiDesigner.core.GridConstraints.FILL_HORIZONTAL, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_SHRINK | com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_GROW, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        TakeExam = new JPanel();
        TakeExam.setLayout(new com.intellij.uiDesigner.core.GridLayoutManager(7, 2, new Insets(0, 0, 0, 0), -1, -1));
        rootPanel.add(TakeExam, new com.intellij.uiDesigner.core.GridConstraints(14, 0, 1, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_CENTER, com.intellij.uiDesigner.core.GridConstraints.FILL_BOTH, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_SHRINK | com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_GROW, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_SHRINK | com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_GROW, null, null, null, 0, false));
        textField5 = new JTextField();
        TakeExam.add(textField5, new com.intellij.uiDesigner.core.GridConstraints(5, 0, 1, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_WEST, com.intellij.uiDesigner.core.GridConstraints.FILL_HORIZONTAL, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_WANT_GROW, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, null, new Dimension(150, -1), null, 0, false));
        textField2 = new JTextField();
        TakeExam.add(textField2, new com.intellij.uiDesigner.core.GridConstraints(2, 0, 1, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_WEST, com.intellij.uiDesigner.core.GridConstraints.FILL_HORIZONTAL, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_WANT_GROW, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, null, new Dimension(150, -1), null, 0, false));
        textField3 = new JTextField();
        TakeExam.add(textField3, new com.intellij.uiDesigner.core.GridConstraints(3, 0, 1, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_WEST, com.intellij.uiDesigner.core.GridConstraints.FILL_HORIZONTAL, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_WANT_GROW, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, null, new Dimension(150, -1), null, 0, false));
        textField4 = new JTextField();
        TakeExam.add(textField4, new com.intellij.uiDesigner.core.GridConstraints(4, 0, 1, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_WEST, com.intellij.uiDesigner.core.GridConstraints.FILL_HORIZONTAL, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_WANT_GROW, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, null, new Dimension(150, -1), null, 0, false));
        textField8 = new JTextField();
        TakeExam.add(textField8, new com.intellij.uiDesigner.core.GridConstraints(2, 1, 1, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_WEST, com.intellij.uiDesigner.core.GridConstraints.FILL_HORIZONTAL, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_WANT_GROW, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, null, new Dimension(150, -1), null, 0, false));
        textField9 = new JTextField();
        TakeExam.add(textField9, new com.intellij.uiDesigner.core.GridConstraints(3, 1, 1, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_WEST, com.intellij.uiDesigner.core.GridConstraints.FILL_HORIZONTAL, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_WANT_GROW, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, null, new Dimension(150, -1), null, 0, false));
        textField10 = new JTextField();
        TakeExam.add(textField10, new com.intellij.uiDesigner.core.GridConstraints(4, 1, 1, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_WEST, com.intellij.uiDesigner.core.GridConstraints.FILL_HORIZONTAL, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_WANT_GROW, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, null, new Dimension(150, -1), null, 0, false));
        textField11 = new JTextField();
        TakeExam.add(textField11, new com.intellij.uiDesigner.core.GridConstraints(5, 1, 1, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_WEST, com.intellij.uiDesigner.core.GridConstraints.FILL_HORIZONTAL, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_WANT_GROW, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, null, new Dimension(150, -1), null, 0, false));
        submitExamButton = new JButton();
        submitExamButton.setText("Submit Exam");
        TakeExam.add(submitExamButton, new com.intellij.uiDesigner.core.GridConstraints(6, 1, 1, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_CENTER, com.intellij.uiDesigner.core.GridConstraints.FILL_HORIZONTAL, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_SHRINK | com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_GROW, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        pleaseAnswerTheQuestionsTextArea = new JTextArea();
        pleaseAnswerTheQuestionsTextArea.setText("Please answer the questions below, remember be honest");
        TakeExam.add(pleaseAnswerTheQuestionsTextArea, new com.intellij.uiDesigner.core.GridConstraints(0, 0, 1, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_CENTER, com.intellij.uiDesigner.core.GridConstraints.FILL_BOTH, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_WANT_GROW, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_WANT_GROW, null, new Dimension(150, 50), null, 0, false));
        textField1 = new JTextField();
        TakeExam.add(textField1, new com.intellij.uiDesigner.core.GridConstraints(1, 0, 1, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_WEST, com.intellij.uiDesigner.core.GridConstraints.FILL_HORIZONTAL, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_WANT_GROW, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, null, new Dimension(150, -1), null, 0, false));
        textField7 = new JTextField();
        TakeExam.add(textField7, new com.intellij.uiDesigner.core.GridConstraints(1, 1, 1, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_WEST, com.intellij.uiDesigner.core.GridConstraints.FILL_HORIZONTAL, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_WANT_GROW, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, null, new Dimension(150, -1), null, 0, false));
        TeacherGradeView = new JPanel();
        TeacherGradeView.setLayout(new com.intellij.uiDesigner.core.GridLayoutManager(2, 2, new Insets(0, 0, 0, 0), -1, -1));
        rootPanel.add(TeacherGradeView, new com.intellij.uiDesigner.core.GridConstraints(15, 0, 1, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_CENTER, com.intellij.uiDesigner.core.GridConstraints.FILL_BOTH, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_SHRINK | com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_GROW, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_SHRINK | com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_GROW, null, null, null, 0, false));
        final com.intellij.uiDesigner.core.Spacer spacer8 = new com.intellij.uiDesigner.core.Spacer();
        TeacherGradeView.add(spacer8, new com.intellij.uiDesigner.core.GridConstraints(1, 0, 1, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_CENTER, com.intellij.uiDesigner.core.GridConstraints.FILL_VERTICAL, 1, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_WANT_GROW, null, null, null, 0, false));
        goBackToDashboardButton1 = new JButton();
        goBackToDashboardButton1.setText("Go back to Dashboard");
        TeacherGradeView.add(goBackToDashboardButton1, new com.intellij.uiDesigner.core.GridConstraints(0, 1, 1, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_CENTER, com.intellij.uiDesigner.core.GridConstraints.FILL_HORIZONTAL, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_SHRINK | com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_GROW, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        textArea2 = new JTextArea();
        TeacherGradeView.add(textArea2, new com.intellij.uiDesigner.core.GridConstraints(0, 0, 1, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_CENTER, com.intellij.uiDesigner.core.GridConstraints.FILL_BOTH, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_WANT_GROW, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_WANT_GROW, null, new Dimension(150, 50), null, 0, false));
    }

    /**
     * @noinspection ALL
     */
    public JComponent $$$getRootComponent$$$() {
        return rootPanel;
    }
}
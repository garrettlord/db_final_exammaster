public class ExamResponse {
    int examResponseID;
    int examID;
    int userID;
    double score;
    boolean submitted;

    public ExamResponse(int examId, double _score) {
        examID = examId;
        score = _score;
    }

    public int examResponseID()
    {
        return examResponseID;
    }

    public void setExamResponseID( int id )
    {
        examResponseID = id;
    }

    public int examID()
    {
        return examID;
    }

    public void setExamID( int id )
    {
        examID = id;
    }

    public double score()
    {
        return score;
    }

    public void setScore( double num )
    {
        score = num;
    }

    public boolean submitted()
    {
        return submitted;
    }

    public void setSubmitted( boolean sub )
    {
        submitted = sub;
    }
}
.SUFFIXES: .java
ASSIGNMENT=ExamMaker
VM=java
CC=javac
CFLAGS= -cp lib:.
EXEC=ExamMaker
SRC=ExamMaker.java

all: $(SRC) $(EXEC)
	
$(EXEC): 
	$(CC) $(CFLAGS) $(SRC)

run:
	$(VM) $(EXEC)

clean:
	rm -f ./*~
	rm -f ./*.class 
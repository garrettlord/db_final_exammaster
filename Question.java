import java.util.*;
import java.sql.*;

public class Question {
    // Properties
    int questionID;
    int examID;
    int rightID;

    String _text;
    String _answer;
    List<Answer> answers;

    public Question(String text, String answer)
    {
       _text = text;
       _answer = answer;
    }

    public int questionID()
    {
        return questionID;
    }

    public void setQuestionID( int id )
    {
        questionID = id;
    }

    public int examID()
    {
        return examID;
    }

    public void setExamID( int id )
    {
        examID = id;
    }

    public int rightID()
    {
        return rightID;
    }

    public void setRightID( int id )
    {
        rightID = id;
    }

    public String text()
    {
        return _text;
    }

    public void setText( String txt )
    {
        _text = txt;
    }
}
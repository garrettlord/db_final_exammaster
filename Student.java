import java.util.*;
import java.sql.*;

public class Student extends User {
    List<Course> courses;
    List<ExamResponse> exams;

    public Student()
    {
        courses = new ArrayList<Course>();
        exams = new ArrayList<ExamResponse>();
        type = "Student";
    }

    public Student(int user, String name)
    {
        userID = user;
        username = name;
        courses = new ArrayList<Course>();
        exams = new ArrayList<ExamResponse>();
        type = "Student";
    }

    public ExamResponse takeExam( Exam ex )
    {
        return null;
    }

    public List<Exam> previousExams()
    {
        return null;
    }
}
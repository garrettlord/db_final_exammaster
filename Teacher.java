import java.util.*;
import java.sql.*;

public class Teacher extends User {
    List<Course> courses;

    public Teacher()
    {
        courses = new ArrayList<Course>();
        type = "Teacher";
    }

    public Teacher( int user, String name )
    {
        userID = user;
        username = name;
        courses = new ArrayList<Course>();
        type = "Teacher";
    }

    public void createExam( String name, String classID )
    {
        // INSERT INTO exams (name, classID) VALUES (name, classID);
    }

    public void lookupStudentGrade( Student std )
    {
        
    }
}
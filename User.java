import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Properties;

public class User {
    // Properties
    int userID;
    String username;
    String type;

    public User()
    {

    }

    public int userID()
    {
        return userID;
    }

    public void setUserID( int id )
    {
        userID = id;
    }
    
    public String username()
    {
        return username;
    }

    public void setUsername( String user )
    {
        username = user;
    }

    public String type()
    {
        return type;
    }

    public void setType( String tp )
    {
        type = tp;
    }
}
create table IF NOT EXISTS `gklord`.`users` (
`userID` int primary key auto_increment ,
`username` char null default null unique,
`pass` varchar(32) ,
`type` char(200)
);

create table IF NOT EXISTS `gklord`.`exams` (
`examID` int primary key auto_increment ,
`courseID` int,
`name` char(200)
);

create table IF NOT EXISTS `gklord`.`questions` (
`questionID` int primary key auto_increment ,
`examID` int,
`question` text,
`answer` text
);

CREATE TABLE  `gklord`.`courses` (
`courseID` INT PRIMARY KEY AUTO_INCREMENT,
`teacherID` INT,
`name` CHAR(200)
);

create table if not exists `gklord`.`student_exams` (
`userID` int,
`examID` int
);

create table IF NOT EXISTS `gklord`.`registrations` (
`userID` int,
`courseID` int
);

create table IF NOT EXISTS `gklord`.`exam_responses` (
`examresponseID` int primary key auto_increment ,
`examID` int,
`userID` int,
`score` double
);

create table IF NOT EXISTS `gklord`.`question_responses` (
`questionresponseid` int primary key auto_increment,
`questionID` int,
`examresponseID` int,
`answer` int,
`correct` bool
);
